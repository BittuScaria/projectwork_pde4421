Plantobot V3.0
Msc Robotics Middlesex university Dubai
PDE4421 
Bittu Scaria, Kiyan Afsari

------------------------------------------

-- How to start: 

1- Place the robot on the map with two IR sensors facing outside the line
2- Switch on the Robot.
3- On Ros, run roscore
4- run rossrial node
5- run python script
6- check rosserial or the leds for outputs



For any questions contact: kiyan.afsari@gmail.com or robotbits007@gmail.com