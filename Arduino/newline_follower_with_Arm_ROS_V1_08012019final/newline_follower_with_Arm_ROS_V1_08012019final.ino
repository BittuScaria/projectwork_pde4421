

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <Servo.h>        // includes servo library for arduino

////////////////////////////////////////////////////////////
/////ros related libraries
#include <ros.h>
#include <ros/time.h>
#include <std_msgs/UInt8.h>
#include <std_msgs/UInt16.h>
#include <sensor_msgs/Range.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Empty.h>
////////////////////////////////////////////////////////////

int speed_vel;
ros::NodeHandle  nh;



Servo servo1;             // assigning servo1
Servo servo2;             // assigning servo2



// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield();


// Select which 'port' M1, M2, M3 or M4. In this case, M1 and M2
Adafruit_DCMotor *myMotor1 = AFMS.getMotor(1);
// You can also make another motor on port M2
Adafruit_DCMotor *myMotor2 = AFMS.getMotor(2);


//LIR-LEFT IR SENSOR------we are not using this currently

//CIR-CENTRAL IR SENSOR

//RIR-RIGHT IR SENSOR


const int RIR=6;
const int CIR=7;
const int LIR=5;
const int ODS=8;

int distance;
int val1=0,val2=0,val3=0,val4=0,val5=0;

int val;
int tempPin = A0;

int lightvalue;
int lightPin = A2;

int moisturevalue;
int moistPin = A1;



void messageCb( const std_msgs::Empty& toggle_msg){
  digitalWrite(44, HIGH-digitalRead(44));   // blink the green LED
}

void message2Cb( const std_msgs::Empty& toggle_msg){
  digitalWrite(46, HIGH-digitalRead(46));   // blink the white LED
}
void message3Cb( const std_msgs::Empty& toggle_msg){
  digitalWrite(48, HIGH-digitalRead(48));   // blink the blue LED
}

void message4Cb( const std_msgs::Empty& toggle_msg){
  digitalWrite(42, HIGH-digitalRead(42));   // blink the white2 LED
}

ros::Subscriber<std_msgs::Empty> sub4("toggle_greenled", &messageCb );

ros::Subscriber<std_msgs::Empty> sub5("toggle_whiteled", &message2Cb );

ros::Subscriber<std_msgs::Empty> sub6("toggle_blueled", &message3Cb );

ros::Subscriber<std_msgs::Empty> sub7("toggle_white2led", &message4Cb );

///////////////////////////////////////////////////////////////////////////
sensor_msgs::Range range_msg;
ros::Publisher pub_range( "/distance", &range_msg);

char frameid[] = "/distance";

const unsigned int TRIG_PIN=13;
const unsigned int ECHO_PIN=12;
const unsigned int BAUD_RATE=57600;

void messageCbUInt8( const std_msgs::UInt8& msg){
  speed_vel=msg.data;
  nh.loginfo( "Received uint8 msg" );
}
std_msgs::UInt8 UInt8_msg;

ros::Subscriber<std_msgs::UInt8> sub3("speed_vel", &messageCbUInt8 );

/////////////////////////////////////////////////////////////////////////

void servo1_cb( const std_msgs::UInt16& cmd_msg){
  servo1.write(cmd_msg.data); //set servo angle, should be from 0-180  
  digitalWrite(50, HIGH-digitalRead(50));  //toggle led  
}


void servo2_cb( const std_msgs::UInt16& cmd_msg){
  servo2.write(cmd_msg.data); //set servo angle, should be from 0-180  
  digitalWrite(50, HIGH-digitalRead(50));  //toggle led  
}


ros::Subscriber<std_msgs::UInt16> sub1("servo1", servo1_cb);
ros::Subscriber<std_msgs::UInt16> sub2("servo2", servo2_cb);





///////////////////////////////////////////////////


std_msgs::Float32 float32_msg;
ros::Publisher pub3_float32("/temperature", &float32_msg);


std_msgs::Float32 float32_msg1;
ros::Publisher pub1_float32("/moisture", &float32_msg);

std_msgs::Float32 float32_msg2;
ros::Publisher pub2_float32("/light", &float32_msg);




/////////////////////////////////////////////////

void setup()

{

pinMode(RIR,INPUT);

pinMode(CIR,INPUT);

pinMode(LIR,INPUT);

pinMode(ODS,INPUT);


 


 pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  Serial.begin(BAUD_RATE);

  pinMode(44,OUTPUT);
    nh.initNode();
    nh.subscribe(sub4);

  pinMode(46,OUTPUT);
    nh.initNode();
    nh.subscribe(sub5);


  pinMode(48,OUTPUT);
    nh.initNode();
    nh.subscribe(sub6);
    
  pinMode(42,OUTPUT);
    nh.initNode();
    nh.subscribe(sub7);
    

 nh.initNode();
    nh.subscribe(sub1);
    nh.subscribe(sub2);

    nh.initNode();
    nh.subscribe(sub3);



  servo1.attach(9) ;  // assigning servo motor pins
    servo2.attach(10);


nh.initNode();
nh.advertise(pub_range);

nh.initNode();
nh.advertise(pub3_float32);

nh.initNode();
nh.advertise(pub1_float32);

nh.initNode();
nh.advertise(pub2_float32);


  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_msg.header.frame_id =  frameid;
  range_msg.field_of_view = 0.1;  // fake
  range_msg.min_range = 0.0;
  range_msg.max_range = 6.47;
  
  pinMode(8,OUTPUT);
  digitalWrite(8, LOW);





  Serial.begin(57600);           // set up Serial library at 57600 bps
  Serial.println("Adafruit Motorshield v2 - DC Motor test!");

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  myMotor1->setSpeed(100);
  myMotor1->run(FORWARD);
   myMotor2->setSpeed(100);
  myMotor2->run(FORWARD);
  // turn on motor
  myMotor1->run(RELEASE);
  myMotor2->run(RELEASE);

}
long range_time;

void loop()
{
 digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  
  
 const unsigned long duration= pulseIn(ECHO_PIN, HIGH);
 int distance= duration/29/2;
 if(duration==0){
   Serial.println("Warning: no pulse from sensor");
   range_time =  millis() + 200;
   } 
  else{
     range_msg.range = distance;
     pub_range.publish(&range_msg);
     range_time =  millis() + 200;
     Serial.print("distance to nearest object:");
     Serial.println(distance);
     Serial.println(" cm");
     val5=digitalRead(distance);
  }

 
{

uint8_t i;

/////uint8_t distance;
i=speed_vel;

///test robot value to move was 38;
val1=digitalRead(RIR);

val2=digitalRead(CIR);

val3=digitalRead(LIR);

val4=digitalRead(ODS);

val5=digitalRead(distance);

Serial.println(val1);
Serial.println(val2);
Serial.println(val3);



//////////////////////////////////////////////////////

val = analogRead(tempPin);
float cel = -0.25*val+75;
Serial.print("TEMPRATURE = ");
Serial.print(cel);
Serial.print("*C");
Serial.println();
delay(10);

float32_msg.data = cel;
      pub3_float32.publish( &float32_msg );
      

//////////////////////////////////////////


moisturevalue = analogRead(moistPin);
float moist = moisturevalue;
Serial.print("MOISTURE = ");
Serial.print(moist);
Serial.print("*level");
Serial.println();
delay(10);

float32_msg.data = moist;
      pub1_float32.publish( &float32_msg );


///////////////////////////////////



lightvalue = analogRead(lightPin);
float light = lightvalue;
Serial.print("LIGHT = ");
Serial.print(light);
Serial.print("*level");
Serial.println();
delay(10);

float32_msg.data = light;
      pub2_float32.publish( &float32_msg );



////////////////////////////////////////////////////////////////////////////





if(val2==HIGH && val1==HIGH)  //go forward straight
{
    myMotor2->setSpeed(i);
  myMotor2->run(FORWARD);
   myMotor1->setSpeed(i);
  myMotor1->run(FORWARD); 
}
else

if(val2==HIGH && val1==LOW)   //left turn
{
   myMotor1->setSpeed(i);
  myMotor1->run(FORWARD);
   myMotor2->setSpeed(i);
  myMotor2->run(BACKWARD);
}

else
if(val2==LOW && val1==HIGH)  //right turn
{
   myMotor2->setSpeed(i);
  myMotor2->run(FORWARD);
   myMotor1->setSpeed(i);
  myMotor1->run(BACKWARD);
}

if(val2==HIGH && val1==HIGH)  //go forward straight
{
    myMotor2->setSpeed(i);
  myMotor2->run(FORWARD);
   myMotor1->setSpeed(i);
  myMotor1->run(FORWARD); 
}

else
if(val2==LOW && val1==LOW) //stop
{
     myMotor2->setSpeed(i);
  myMotor2->run(RELEASE);
   myMotor1->setSpeed(i);
  myMotor1->run(RELEASE);
}


}

delay(10);
nh.spinOnce();
}
