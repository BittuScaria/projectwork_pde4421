#!/usr/bin/env python


#Program details

#This project is made to test an ULTRASONIC(SONIC) SENSOR with ARduino publishing the range to ROS and by a ROS python node we subscribe the data and based on the sensor input ,we make the turtle to react on virtual/simulation world.
#Running Phase 4 project
#run roscore on Terminal 1------starting the rose engine
#open Arduino IDE ,,open ,verify and Uplaod  program under Arduino IDE folder
#Terminal 2 run  $ rosrun rosserial_python serial_node.py /dev/ttyACM0 ------------to start the rosserial communication with arduino
#terminal 3 $ rosrun dcmotor_with_ultrasonicsensor ultrasonic_dcmotor.py
 
#we can see the turtle moves straight then rotates and again moves traight if no objects are detected in front of the ultrasonic sensor in real world.If you place your hand less than 10cm distance turtle will keep on rotating untill you move your hand out of the sensor.And after detecting the object turtle changes its direction of movement.



#Refernce documents and webistes as follows;
#http://wiki.ros.org/
#http://wiki.ros.org/rosserial_arduino/Tutorials



import rospy
import time
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Range
from std_msgs.msg import UInt8
from std_msgs.msg import UInt16


###def move_callback(speedvalue):
	##global move_value
	##move_value = msg.UInt8
##sonar_head = 22 # anything to star




def scan_callback(msg):
	global sonar_head
	sonar_head = msg.range
sonar_head = 22 # anything to star

scan_sub = rospy.Subscriber('distance', Range, scan_callback)
speed_vel_pub = rospy.Publisher('speed_vel', UInt8, queue_size=1)
servo1_pub = rospy.Publisher('servo1', UInt16, queue_size=1)
servo2_pub = rospy.Publisher('servo2', UInt16, queue_size=1)
rospy.init_node('ultrasonic_dcmotor')
state_change_time = rospy.Time.now()
driving_forward = True
rate = rospy.Rate(10)
while not rospy.is_shutdown():
	if driving_forward:
		if (sonar_head < 10.0 ):
			driving_forward = False
			
	else: # we're not driving_forward
		if (sonar_head > 10.0 )::
			driving_forward = True # we're done spinning, time to go forward!
			
		speed = UInt8()
		servo1value = UInt16()
		servo2value = UInt16()
		if driving_forward:
			servo1value.data = 0
			servo2value.data = 0
			speed.data = 45
		else:
			speed.data = 0
			speed_vel_pub.publish(speed)
			time.sleep(2) 
			servo1value.data = 60
			servo1_pub.publish(servo1value)
			time.sleep(2) 
			servo2value.data = 70
			servo2_pub.publish(servo2value)
			time.sleep(2) 
			servo2value.data = 0
			servo2_pub.publish(servo2value)
			time.sleep(2) 
			servo2value.data = 70
			servo2_pub.publish(servo2value)
			time.sleep(2) 
			servo2value.data = 0
			servo2_pub.publish(servo2value)
			servo1value.data = 0
			servo1_pub.publish(servo1value)
			time.sleep(2) 

		speed_vel_pub.publish(speed)
		rate.sleep()
