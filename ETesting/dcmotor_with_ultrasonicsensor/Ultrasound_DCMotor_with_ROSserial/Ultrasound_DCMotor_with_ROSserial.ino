/* 
 *  program made by Bittu Scaria MIddlesex University
 * program to test the adafruit V2 sheild with ROS Serial INterface
 * program sucessfully tested on 18/11/2018.
 * roscore on terminal 1
 * ros serial communication on terminal 2 "rosrun rosserial_python serial_node.py /dev/ttyACM0"
 * on terminal 3 rostopic pub speed_value std_msgs/UInt8 (speed value for the motors)

 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <ros.h>
#include <ros/time.h>
#include <std_msgs/UInt8.h>
#include <sensor_msgs/Range.h>

int speed_vel;
ros::NodeHandle  nh;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

Adafruit_DCMotor *myMotor1 = AFMS.getMotor(1);
Adafruit_DCMotor *myMotor2 = AFMS.getMotor(2);


sensor_msgs::Range range_msg;
ros::Publisher pub_range( "/ultrasound", &range_msg);

char frameid[] = "/ultrasound";

const unsigned int TRIG_PIN=13;
const unsigned int ECHO_PIN=12;
const unsigned int BAUD_RATE=57600;

void messageCbUInt8( const std_msgs::UInt8& msg){
  speed_vel=msg.data;
  nh.loginfo( "Received uint8 msg" );
}
std_msgs::UInt8 UInt8_msg;

ros::Subscriber<std_msgs::UInt8> sub3("speed_vel", &messageCbUInt8 );





void setup() {

pinMode(TRIG_PIN, OUTPUT);
pinMode(ECHO_PIN, INPUT);
Serial.begin(57600);
    
    nh.initNode();
    nh.subscribe(sub3);

nh.initNode();
nh.advertise(pub_range);

  range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  range_msg.header.frame_id =  frameid;
  range_msg.field_of_view = 0.1;  // fake
  range_msg.min_range = 0.0;
  range_msg.max_range = 6.47;
  
  pinMode(8,OUTPUT);
  digitalWrite(8, LOW);
 

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  myMotor1->setSpeed(100);
  myMotor1->run(FORWARD);
   myMotor2->setSpeed(100);
  myMotor2->run(FORWARD);
  // turn on motor
  myMotor1->run(RELEASE);
  myMotor2->run(RELEASE);
}

long range_time;

void loop() {

digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  
 const unsigned long duration= pulseIn(ECHO_PIN, HIGH);
 int ultrasound = duration/29/2;
 if(duration==0){
   //Serial.println("Warning: no pulse from sensor");
   
    range_time =  millis() + 200;
  }
  else{

    range_msg.range = ultrasound;
    pub_range.publish(&range_msg);
    range_time =  millis() + 200;
  }
  
  uint8_t i;
  i=speed_vel;

  myMotor1->run(FORWARD);
  myMotor2->run(FORWARD);
  
  {
  i=speed_vel;
    myMotor1->setSpeed(i);  
    myMotor2->setSpeed(i);
     delay(100) ;
  }
  


nh.spinOnce();
  delay(100);
}
