/* 
 *  program made by Bittu Scaria MIddlesex University
 * program to test the adafruit V2 sheild with ROS Serial INterface
 * program sucessfully tested on 18/11/2018.
 * roscore on terminal 1
 * ros serial communication on terminal 2 "rosrun rosserial_python serial_node.py /dev/ttyACM0"
 * on terminal 3 rostopic pub speed_value std_msgs/UInt8 (speed value for the motors)

 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <ros.h>
#include <std_msgs/UInt8.h>

int speed_value;
ros::NodeHandle  nh;

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

Adafruit_DCMotor *myMotor1 = AFMS.getMotor(1);
Adafruit_DCMotor *myMotor2 = AFMS.getMotor(2);

void messageCbUInt8( const std_msgs::UInt8& msg){
  speed_value=msg.data;
  nh.loginfo( "Received uint8 msg" );
}
std_msgs::UInt8 UInt8_msg;

ros::Subscriber<std_msgs::UInt8> sub3("speed_value", &messageCbUInt8 );



void setup() {
    nh.initNode();
    nh.subscribe(sub3);
 

  AFMS.begin();  // create with the default frequency 1.6KHz
  //AFMS.begin(1000);  // OR with a different frequency, say 1KHz
  
  // Set the speed to start, from 0 (off) to 255 (max speed)
  myMotor1->setSpeed(100);
  myMotor1->run(FORWARD);
   myMotor2->setSpeed(100);
  myMotor2->run(FORWARD);
  // turn on motor
  myMotor1->run(RELEASE);
  myMotor2->run(RELEASE);
}

void loop() {
  uint8_t i;
  i=speed_value;

  myMotor1->run(FORWARD);
  myMotor2->run(FORWARD);
  
  {
  i=speed_value;
    myMotor1->setSpeed(i);  
    myMotor2->setSpeed(i);
     delay(1000) ;
  }
  myMotor1->run(RELEASE);
  myMotor2->run(RELEASE);
  delay(2000);


nh.spinOnce();
  delay(100);
}
